# Installation
> `npm install --save @types/tap`

# Summary
This package contains type definitions for tap (https://github.com/tapjs/node-tap).

# Details
Files were exported from https://github.com/DefinitelyTyped/DefinitelyTyped/tree/master/types/tap.

### Additional Details
 * Last updated: Tue, 07 Nov 2023 15:11:36 GMT
 * Dependencies: [@types/node](https://npmjs.com/package/@types/node)

# Credits
These definitions were written by [zkldi](https://github.com/zkldi).
